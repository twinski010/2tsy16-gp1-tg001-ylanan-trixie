﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_9
{
    class Character
    {
        //Constructor

        public Character(string name, float hp, float xp, float atk)
        {
            Name = name;
            CurrentHP = hp;
            MaximumHP = hp;
            exp = xp;
            atkdamage = atk;
        }

        public string Name;
        public float CurrentHP;
        public float MaximumHP;
        private float exp;
        public float atkdamage;

        public float MaxHP
        {
            get
            {
                return MaximumHP;
            }

            set
            {
                MaximumHP = value;
            }
        }

        public float CurrHP
        {
            get
            {
                return CurrentHP;
            }

            set
            {
                CurrentHP = value;
            }
        }

        public float Exp
        {
            get
            {
                return exp;
            }

            set
            {
                exp = value;
                if (exp > 0)
                    exp = 0;
            }
        }
    }
}

