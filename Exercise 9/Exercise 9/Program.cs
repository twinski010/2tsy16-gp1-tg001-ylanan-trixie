﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_9
{
    class Program
    {
        static void Attack(string name, float attacker, float victim, string name2, float attacker2, float victim2)
        {
            do
            {
                victim = victim - attacker;
                Console.WriteLine(name + "'s HP: " + victim);

                victim2 = victim2 - attacker2;
                Console.WriteLine(name2 + "'s HP: " + victim2 + "\n");
            }
            while (victim > 0 && victim2 > 0);


            if (victim <= 0)
            {
                Console.WriteLine("Monster Wins!");
            }

            else if (victim2 <= 0)
            {
                Console.WriteLine("Hero Wins!");
            }
        }

        static void Main(string[] args)
        {
            Character hero = new Character("Bob", 80, 0, 30);
            Monster mon = new Monster("Dragon", 100, 0, 10);

            Attack(hero.Name, mon.atkdamage, hero.CurrentHP, mon.Name, hero.atkdamage, mon.CurrentHP);
           
            Console.ReadLine();
        }
    }
}
