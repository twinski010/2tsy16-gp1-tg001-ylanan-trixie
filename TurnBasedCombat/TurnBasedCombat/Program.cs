﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedCombat
{
    class Program
    {
        // You don't need to do anything here
        static void Main(string[] args)
        {
            // Prepare team
            Team player = CreatePlayerTeam();
            Team enemy = CreateEnemyTeam();

            // Initiate combat
            Combat combat = new Combat();
            combat.Player = player;
            combat.Enemy = enemy;
            combat.Initiate(); // This will not finish until a winner is determined

            // Combat has ended at this point
            Console.ReadKey();
        }

        /// <summary>
        /// Create a Team of PlayerUnits with all their Actions. Use the functions already available in the supplied classes
        /// To create an Action, use the constructor and pass in the Unit who owns that Action
        /// Remember that you need to instantiate an action for each unit. Do not reuse the same instance for all units.
        /// </summary>
        /// <returns>Player Team</returns>
        static Team CreatePlayerTeam()
        {
            Team team = new Team();
            team.Name = "Casual";
           
            PlayerUnit unit1 = new PlayerUnit();
            Stats stats1 = new Stats();
            unit1.Name = "Bob";
            unit1.Job = Job.Warrior;
            unit1.Stats = stats1;
            unit1.Team = team;
            stats1.Hp = 48;
            stats1.Mp = 17;
            stats1.Power = 15;
            stats1.Vitality = 14;
            stats1.Agility = 16;
            stats1.Dexterity = 16;
            team.AddMember(unit1);

            PlayerUnit unit2 = new PlayerUnit();
            Stats stats2 = new Stats();
            unit2.Name = "Mark";
            unit2.Job = Job.Mage;
            unit2.Stats = stats2;
            unit2.Team = team;
            stats2.Hp = 43;
            stats2.Mp = 25;
            stats2.Power = 12;
            stats2.Vitality = 11;
            stats2.Agility = 13;
            stats2.Dexterity = 15;
            team.AddMember(unit2);

            PlayerUnit unit3 = new PlayerUnit();
            Stats stats3 = new Stats();
            unit3.Name = "Jeff";
            unit3.Job = Job.Assassin;
            unit3.Stats = stats3;
            unit3.Team = team;
            stats3.Hp = 45;
            stats3.Mp = 20;
            stats3.Power = 14;
            stats3.Vitality = 13;
            stats3.Agility = 19;
            stats3.Dexterity = 17;
            team.AddMember(unit3);

            return CreatePlayerTeam();
        }

        /// <summary>
        /// Create a Team of EnemyUnits with all their Actions. Use the functions already available in the supplied classes
        /// To create an Action, use the constructor and pass in the Unit who owns that Action.
        /// Remember that you need to instantiate an action for each unit. Do not reuse the same instance for all units.
        /// </summary>
        /// <returns>Enemy Team</returns>
        static Team CreateEnemyTeam()
        {
            Team team = new Team();
            team.Name = "Hardcore";
            
            PlayerUnit unit1 = new PlayerUnit();
            Stats stats1 = new Stats();
            unit1.Name = "Xander";
            unit1.Job = Job.Warrior;
            unit1.Stats = stats1;
            unit1.Team = team;
            stats1.Hp = 50;
            stats1.Mp = 15;
            stats1.Power = 14;
            stats1.Vitality = 13;
            stats1.Agility = 14;
            stats1.Dexterity = 15;
            team.AddMember(unit1);

            PlayerUnit unit2 = new PlayerUnit();
            Stats stats2 = new Stats();
            unit2.Name = "Leo";
            unit2.Job = Job.Mage;
            unit2.Stats = stats2;
            unit2.Team = team;
            stats2.Hp = 45;
            stats2.Mp = 22;
            stats2.Power = 13;
            stats2.Vitality = 12;
            stats2.Agility = 12;
            stats2.Dexterity = 12;
            team.AddMember(unit2);

            PlayerUnit unit3 = new PlayerUnit();
            Stats stats3 = new Stats();
            unit3.Name = "Marx";
            unit3.Job = Job.Assassin;
            unit3.Stats = stats3;
            unit3.Team = team;
            stats3.Hp = 42;
            stats3.Mp = 20;
            stats3.Power = 15;
            stats3.Vitality = 12;
            stats3.Agility = 16;
            stats3.Dexterity = 18;
            team.AddMember(unit3);

            return CreateEnemyTeam();
        }
    }
}
