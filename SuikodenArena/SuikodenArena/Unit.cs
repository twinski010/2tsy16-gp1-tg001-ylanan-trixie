﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuikodenArena
{
    /// <summary>
    /// Possible actions a unit can execute. Here's an example on how to use this
    /// Some examples:
    /// Ex#1 Action action = ActionType.Attack
    /// Ex#2 if (action == ActionType.Attack)
    /// Ex#3 return ActionType.Attack
    /// </summary>
    public enum ActionType
    {
        None,
        Attack,
        Defend,
        WildAttack
    }

    public class Unit
    {
        /// <summary>
        /// Use this object to randomize value. This will prevent getting the same values.
        /// To use, call this function: random.Next(int maxvalue) or random.Next(int minValue, int maxValue);
        /// </summary>
        private static readonly Random random = new Random();

        public string Name { get; set; }
        public int Power { get; set; }
        public int Defense { get; set; }
        public int MaxHp { get; set; }

        /// <summary>
        /// Return true if CurrentHp is greater than 0. False, otherwise.
        /// </summary>
        public bool Alive
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// The variable being set by SetUserAction and SetRandomAction (please refer to these functions). DON'T MODIFY THIS LINE
        /// </summary>
        public ActionType Action { get; private set; }

        /// <summary>
        /// Write an appropriate algorithm for clamping the HP between 0 and MaxHp
        /// For getter, simply return 'currentHp'
        /// For setter, update 'currentHp' value. Make sure that 'currentHp' is set to a value between 0 and MaxHp
        /// </summary>
        private int currentHp;
        public int CurrentHp
        {
            // Don't change this
            get { return currentHp; }

            // Implement this
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Randomize damage based on Power.
        /// Minimum damage = Your Power
        /// Maximum damage = 120% of your power (eg. if Power = 100, then max damage = 120)
        /// </summary>
        /// <returns></returns>
        public int RandomizeDamage()
        {
            int randaction = random.Next(Power, Power+20);
            Power = randaction;
            return Power;
        }

        /// <summary>
        /// Evaluate this unit's action and the target's action. Based on the action, deal damage with appropriate damage modifier (if applicable). 
        /// This function only tries to damage the target from the perspective of this unit. Target's damage will be evaluated on the target's side.
        /// Refer to the matchup table in the spec for damage modifiers.
        /// TIP: Read this.Action and target.Action and compare these two.
        /// </summary>
        /// <param name="target"></param>
        public void EvaluateAction(Unit target)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Call this to set action based from user input.
        /// Ask the user for input and return an enum value.
        /// After determining the action, set the Action variable based on evaluated value.
        /// TIP: Ask the player to input a number. Where a number is tied to one action
        ///     1 = ActionType.Attack
        ///     2 = ActionType.Defend
        ///     3 = ActionType.WildAttack
        /// </summary>
        public void SetUserAction()
        {
            ActionType action = new ActionType();

            Console.WriteLine("Choose an action:");
            Console.WriteLine("\n[1] Attack");
            Console.WriteLine("\n[2] Defend");
            Console.WriteLine("\n[3] Wild Attack\n");

            string input = Console.ReadLine();

            if (input.Equals("1"))
            {
                action = ActionType.Attack;
            }

            else if (input.Equals("2"))
            {
                action = ActionType.Defend;
            }

            else if (input.Equals("3"))
            {
                action = ActionType.WildAttack;
            }
        }

        /// <summary>
        /// Call this to get action from AI (randomized)
        /// You can randomize 3 numbers where each number is tied to one action
        ///     1 = ActionType.Attack
        ///     2 = ActionType.Defend
        ///     3 = ActionType.WildAttack
        /// </summary>
        /// <returns>A randomized action</returns>
        public void SetRandomAction()
        {
            int randaction = random.Next(1, 3);
            ActionType action = new ActionType();

            if (randaction == 1)
            {
                action = ActionType.Attack;
            }

            else if (randaction == 2)
            {
                action = ActionType.Defend;
            }

            else if (randaction == 3)
            {
                action = ActionType.WildAttack;
            }
        }

        /// <summary>
        /// Display the name, HP, and stats of this unit (Console.WriteLine)
        /// </summary>
        public void DisplayStats()
        {
            throw new NotImplementedException();
            Party party = new Party();

        }
    }
}
