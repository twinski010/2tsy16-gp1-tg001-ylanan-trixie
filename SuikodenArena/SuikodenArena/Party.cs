﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuikodenArena
{
    public class Party
    {
        /// <summary>
        /// Use this to give the party some kind of identity
        /// </summary>
        public string Name { get; set;}

        // DO NOT CHANGE THESE 2 LINES
        private List<Unit> units = new List<Unit>();
        public IEnumerable<Unit> Units { get { return units; } }

        /// <summary>
        /// If there's no more units left (units list count is 0), return true. False, otherwise
        /// </summary>
        public bool Wiped
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Add the unit to the 'units' list
        /// </summary>
        /// <param name="unit"></param>
        public void AddUnit(Unit unit)
        {
           Unit u1 = new Unit();
            u1.Name = "Viktor";
            u1.Power = 14;
            u1.Defense = 7;
            u1.MaxHp = 37;
            units.Add(u1);

            Unit u2 = new Unit();
            u2.Name = "Clive";
            u2.Power = 15;
            u2.Defense = 5;
            u2.MaxHp = 34;
            units.Add(u2);

            Unit u3 = new Unit();
            u3.Name = "Oulan";
            u3.Power = 10;
            u3.Defense = 5;
            u3.MaxHp = 36;
            units.Add(u3);

            Unit u4 = new Unit();
            u4.Name = "Nanami";
            u4.Power = 18;
            u4.Defense = 5;
            u4.MaxHp = 39;
            units.Add(u4);

            Unit u5 = new Unit();
            u5.Name = "Hero";
            u5.Power = 16;
            u5.Defense = 5;
            u5.MaxHp = 32;
            units.Add(u5);
        }

        /// <summary>
        /// Get the next unit in queue. This unit is the index 0 of the 'units' list. 
        /// Remove the returned unit from the list.
        /// </summary>
        /// <returns>If the list is empty, return null. Returns the unit at index 0 if the list has at least one unit.</returns>
        public Unit NextUnit()
        {
            throw new NotImplementedException();
            units.RemoveAt(0);
        }

        /// <summary>
        /// Print team name and all the units in the party (Console.WriteLine)
        /// </summary>
        public void DisplayParty()
        {
            Party party = new Party();
            party.Name = "108 Stars of Destiny";
            Console.WriteLine(party.Name);
            Console.WriteLine("=================================================================");
            Console.WriteLine(units);
        }
    }
}

