﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] items = { "Health Potion", "Mana Potion", "Yggdrasil leaf", "Dreadwyrm Staff", "Holy Robe" };

            Console.WriteLine("Input an item to search: ");

            string input = Console.ReadLine();


            for (int i = 0; i < items.Length; i++)
            {
                if (input.Equals(items[i]))
                {
                    Console.WriteLine("\n");
                    Console.WriteLine(input + " was found in the array.");
                }

                else
                {
                    Console.WriteLine("\n");
                    Console.WriteLine(input + " was not found in the array.");
                }
            }

            Console.ReadKey();
        }

    }
}
