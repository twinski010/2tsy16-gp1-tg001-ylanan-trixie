﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_10
{
    class Mage
    {
        public int power { get; set; }
        private int CurrentHP { get; set; }
        private int MaximumHP { get; set; }
        private float exp;
        private int ManaPoints;
        private int MaxManaPoints;

        public Weapon Weapon { get; private set; }

        public void EquipWeapon(Weapon weapon)
        {
            if (weapon.Name == "Bow") return;
            if (weapon.Name == "Sword") return;
            Weapon = weapon;
        }

        public float MaxHP
        {
            get
            {
                return MaximumHP;
            }

            set
            {
            }
        }

        public float CurrHP
        {
            get
            {
                return CurrentHP;
            }

            set
            {
            }
        }

        public float Exp
        {
            get
            {
                return exp;
            }

            set
            {
                exp = value;
            }
        }

        public void Die()
        {
            if (CurrentHP <= 0) return;
        }

        public void Cast()
        {
            if (ManaPoints <= 0) return;
        }
    }
}
