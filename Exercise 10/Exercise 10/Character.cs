﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_10
{
    class Character
    {
        public string Name { get; set; }
        private Weapon _weapon;

        public Weapon Weapon
        {
            get { return _weapon; }

            set
            {
                if (value.Name == "Bow") return;
                _weapon = value;
            }
        }
    }
}
