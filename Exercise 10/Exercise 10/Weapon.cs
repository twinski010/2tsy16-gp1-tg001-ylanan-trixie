﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_10
{
    class Weapon
    {
        public string Name { get; set; }
        public int Power { get; set; }
    }
}
