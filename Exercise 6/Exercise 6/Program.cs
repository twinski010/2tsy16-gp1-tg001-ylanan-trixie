﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_6
{
    class Program
    {

        static void PrintArray(string[] items)
        {
            foreach (string item in items)
            {
                Console.WriteLine(item);
            }
        }


        static bool SearchArray(string[] items, string itemToSearch)
        {
            bool found = false;

            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == itemToSearch)
                {
                    found = true;
                    break;
                }
            }
            return found;
        }


        static void ItemQty(int[] itemsqty)
        {
            foreach (int qty in itemsqty)
            {
                Console.WriteLine("Found " + qty + " " + itemsqty + "s.");
                break;
            }
        }


        static void LowestIndex(List<int> hp)
        {
            int minnumber = hp.Min();
            int index = hp.IndexOf(minnumber);
            Console.WriteLine(index);
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Choose a function: ");
            Console.WriteLine("[a] - Print Array");
            Console.WriteLine("[b] - Search Array");
            Console.WriteLine("[c] - Search Array (only a copy of [b])");
            Console.WriteLine("[d] - Search the Quantity of an item in Array");
            Console.WriteLine("[e] - Return Index of Lowest Number");
            Console.WriteLine(" ");
            Console.WriteLine("Input your choice: ");

            string[] items = { "Health Potion", "Mana Potion", "Yggdrasil leaf", "Dreadwyrm Staff", "Holy Robe" };

            int[] itemsqty = { 50, 10, 200, 4 };

            List<int> hp = new List<int>();
            hp.Add(14);
            hp.Add(3);
            hp.Add(50);
            hp.Add(100);
            hp.Add(6);


            string choice = Console.ReadLine();

            if (choice.Equals("a"))

            {
                Console.WriteLine(" ");
                PrintArray(items);
            }


            else if (choice.Equals("b"))
            {
                Console.WriteLine(" ");
                Console.WriteLine("Input an item to search: ");
                string itemtosearch = Console.ReadLine();

                bool found = SearchArray(items, itemtosearch);
              
                    if (found)
                    {
                        Console.WriteLine("Found " + itemtosearch);
                    }

                    else
                    {
                        Console.WriteLine(itemtosearch + " was not found.");   
                    }
             }


            else if (choice.Equals("c"))
            {
                Console.WriteLine(" ");
                Console.WriteLine("Input an item to search: ");
                string itemtosearch = Console.ReadLine();

                bool found = SearchArray(items, itemtosearch);

                if (found)
                {
                    Console.WriteLine("Found " + itemtosearch);
                }

                else
                {
                    Console.WriteLine(itemtosearch + " was not found.");
                }         
            }


            else if (choice.Equals("d"))
            {
                Console.WriteLine(" ");
                Console.WriteLine("Input an item to search: ");
                string itemtosearch = Console.ReadLine();

                Console.WriteLine(" ");
                ItemQty(itemsqty);
            }


            else if (choice.Equals("e"))
            {
                Console.WriteLine(" ");
                LowestIndex(hp);
            }


            else
            {
                Console.WriteLine("\n");
                Console.WriteLine("Choice Invalid.");
            }

            Console.ReadLine();
        }
    }
}
