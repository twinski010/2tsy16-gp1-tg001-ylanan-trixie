﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_12
{
    class Ability
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public float Cooldown { get; private set; }
        public float ManaCost { get; private set; }

        public void Use()
        {

        }
    }
}
