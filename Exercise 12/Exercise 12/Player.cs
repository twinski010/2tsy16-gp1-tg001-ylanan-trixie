﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_12
{
    class Player: Base
    {
        Monster monster = new Monster();

        public void HealMagic()
        {
            CurrentHP += 20;
        }

        public void AttackMagic()
        {
            monster.CurrentHP -= ATKPower;
        }

        public void AttackBuffMagic()
        {
            ATKPower += 10;
        }

        public void DefenseBuffMagic()
        {
            Defense += 10;
        }
    }
}
