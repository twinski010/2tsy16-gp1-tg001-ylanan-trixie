﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class AI : Commander
    {
        /// <summary>
        /// 30% chance to discard, 70% chance to play a card.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            bool triggered = RandomHelper.Chance(0.3f);

            if (triggered)
            {
                Discard();
            }

            else
            {
                PlayCard();
            }
        }

        public override Card PlayCard()
        {
            Player player = new Player();

            Mage mage = new Mage();
            mage.Type = "Mage";

            Assassin assassin = new Assassin();
            assassin.Type = "Assassin";

            Warrior warrior = new Warrior();
            warrior.Type = "Warrior";

            int triggered3 = RandomHelper.Range(0, 2);

            if (triggered3.Equals(0))
            {
                mage.Fight(player.PlayCard());
            }

            else if (triggered3.Equals(1))
            {
                assassin.Fight(player.PlayCard());
            }

            else if (triggered3.Equals(2))
            {
                warrior.Fight(player.PlayCard());
            }

            return null;
        }
    }
}
