﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Player : Commander
    {
        /// <summary>
        /// Get player's input. Player can only either Play or Discard.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            DisplayCards();

            Console.WriteLine("\n[a] - Play");
            Console.WriteLine("[b] - Discard");

            string input = Console.ReadLine();

            if (input.Equals("a"))
            {
                PlayCard();
            }

            else if (input.Equals("b"))
            {
                Discard();
            }
        }

        /// <summary>
        /// Show a list of cards to choose from. If there are 2 cards of the same type (eg. Warrior), only show one of each type.
        /// </summary>
        /// <returns></returns>
        public override Card PlayCard()
        {
            Console.WriteLine("Pick a card:");

            string choice = Console.ReadLine();

                Mage mage = new Mage();
                mage.Type = "Mage";

                Assassin assassin = new Assassin();
                assassin.Type = "Assassin";

                Warrior warrior = new Warrior();
                warrior.Type = "Warrior";

                AI ai = new AI();


                if (choice.Equals(mage.Type))
                {
                    mage.Fight(ai.PlayCard());
                }

                else if (choice.Equals(assassin.Type))
                {
                    assassin.Fight(ai.PlayCard());
                }

                else if (choice.Equals(warrior.Type))
                {
                    warrior.Fight(ai.PlayCard());
                }
            
            return null;
        }
    }
}
