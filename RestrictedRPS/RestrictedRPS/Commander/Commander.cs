﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public abstract class Commander
    {
        public string Name { get; set; }
        public int Points { get; protected set; }

        private List<Card> cards = new List<Card>();
        public IList<Card> Cards { get { return cards; } }
        public bool EmptyHand { get { return cards.Count == 0; } }

        /// <summary>
        /// Can only discard if commander has at least 2 cards.
        /// </summary>
        public bool CanDiscard
        {
            get
            {
                return Cards.Count >= 2;
            }
        }

        /// <summary>
        /// Draw a random card and add it to your hand.
        /// </summary>
        /// <returns></returns>
        public Card Draw()
        {
            int rand = RandomHelper.Range(0, 2);

            if (rand.Equals(0))
            {
                Cards.Add(new Warrior());
            }

            else if (rand.Equals(1))
            {
                Cards.Add(new Mage());
            }

            else if (rand.Equals(2))
            {
                Cards.Add(new Assassin());
            }

            return null;
        }

        /// <summary>
        /// Discards two random cards in exchange of one random card. Cannot discard if player has only one card (throws an exception)
        /// </summary>
        /// <returns>Received card after discarding.</returns>
        public Card Discard()
        {
            if (!CanDiscard) throw new Exception("Cannot discard now."); // DON'T DELETE THIS LINE

            int rand = RandomHelper.Range(0, 5);
            int rand2 = RandomHelper.Range(6, 9);

            if (CanDiscard)
            {
                if (rand.Equals(Cards[rand]) && rand2.Equals(Cards[rand2]))
                {
                    Card next = Cards[rand];
                    Cards.Remove(next);

                    Card next2 = Cards[rand2];
                    Cards.Remove(next2);
                }

                Draw();
            }

            return Draw();
        }

        /// <summary>
        /// Display this commander's cards. NOTE: Only call this for the player's turn.
        /// </summary>
        public void DisplayCards()
        {
            foreach (Card card in Cards)
            {
                Console.WriteLine(card);
            }
        }

        /// <summary>
        /// Called whenever one side's hand is empty. All cards will be discarded. Each discarded card will reduce the player's point by 1.
        /// </summary>
        public void OnGameEnding()
        {
            foreach (Card card in Cards)
            {
                Points--;
            }
        }

        /// <summary>
        /// Commander's action upon his turn. Can either "Play" or "Discard"
        /// </summary>
        /// <param name="opponent"></param>
        public abstract void EvaluateTurn(Commander opponent);

        /// <summary>
        /// Choose a card to play. Card must be discarded after playing.
        /// </summary>
        /// <returns>Card to play.</returns>
        public abstract Card PlayCard();

        /// <summary>
        /// Each commander plays a card. Points are evaluated here.
        /// Free code :)
        /// </summary>
        /// <param name="opponent"></param>
        public void Fight(Commander opponent)
        {
            Card myCard = PlayCard();
            Card opponentCard = opponent.PlayCard();

            FightResult result = myCard.Fight(opponentCard);
            if (result == FightResult.Win)
            {
                Points += 2;
            }
        }
    }
}
