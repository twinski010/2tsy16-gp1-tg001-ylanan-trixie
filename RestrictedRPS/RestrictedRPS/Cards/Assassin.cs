﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Assassin : Card
    {
        public override FightResult Fight(Card opponentCard)
        {
            if (opponentCard.Type == "Assassin")
            {
                return FightResult.Draw;
            }

            else if (opponentCard.Type == "Warrior")
            {
                return FightResult.Lose;
            }

            else return FightResult.Win;
        }
    }
}
