﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Mage : Card
    {
        public override FightResult Fight(Card opponentCard)
        {
            if (opponentCard.Type == "Mage")
            {
                return FightResult.Draw;
            }

            else if (opponentCard.Type == "Assassin")
            {
                return FightResult.Lose;
            }

            else return FightResult.Win;
        }
    }
}
