﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_3
{
    class Program
    {
        static bool SearchArray(string[] items, string itemToSearch)
        {
            string input = itemToSearch;

            for (int i = 0; i < items.Length; i++)
            {
                if (input.Equals(items[i]))
                {
                    Console.WriteLine("\n");
                    Console.WriteLine(input + " was found in the array.");
                    break;
                }

                else
                {
                    Console.WriteLine("\n");
                    Console.WriteLine(input + " was not found in the array.");
                }
            }

            return true;
        }

        static void Main(string[] args)
        {
            string[] items = { "Robe", "Armor", "Plate" };

            Console.WriteLine("Input an item to search: ");

            string input = Console.ReadLine();

            SearchArray();

            Console.ReadKey();
        }
            
    }
}
