﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz_1___TradingPost
{
    class Program
    {
        struct Order
        {
            public string Name;
            public double Price;
        }

        static List<Order> GetDefaultOrders()
        {
            List<Order> defaultOrders = new List<Order>();

            Order o1 = new Order();
            o1.Name = "Rattlesnake";
            o1.Price = 378999988;
            defaultOrders.Add(o1);

            Order o2 = new Order();
            o2.Name = "Rattlesnake";
            o2.Price = 377577659;
            defaultOrders.Add(o2);

            Order o3 = new Order();
            o3.Name = "Rattlesnake";
            o3.Price = 414999900;
            defaultOrders.Add(o3);

            Order o4 = new Order();
            o4.Name = "Rattlesnake";
            o4.Price = 398800000;
            defaultOrders.Add(o4);

            Order o5 = new Order();
            o5.Name = "Drake";
            o5.Price = 48000000;
            defaultOrders.Add(o5);

            Order o6 = new Order();
            o6.Name = "Drake";
            o6.Price = 51975000;
            defaultOrders.Add(o6);

            Order o7 = new Order();
            o7.Name = "Drake";
            o7.Price = 49999999;
            defaultOrders.Add(o7);

            Order o8 = new Order();
            o8.Name = "PLEX";
            o8.Price = 1180000000;
            defaultOrders.Add(o8);

            Order o9 = new Order();
            o9.Name = "PLEX";
            o9.Price = 1177566613;
            defaultOrders.Add(o9);

            Order o10 = new Order();
            o10.Name = "PLEX";
            o10.Price = 1182899876;
            defaultOrders.Add(o10);

            return defaultOrders;
        }

        // Quiz Item
        // Return a list of items that match your item name
        static List<Order> FilterOrders(List<Order> orders, string filterKey)
        {
            for (int i = 0; i < orders.Count; i++)
            {
                if (filterKey.Equals(orders[i].Name))
                {
                    Console.WriteLine(orders[i].Name);
                }
            }
            return orders;
        }

        // Quiz Item
        // Return a list of ordered items by price
        static List<Order> SortOrders(List<Order> orders, string filterKey)
        {
                for (int i = 0; i < orders.Count; i++)
                {
                    if (filterKey.Equals(orders[i].Name))
                    {
                        Console.WriteLine(orders[i].Price);
                        break;
                    }
                }
                return orders;
            }
        
        // Quiz Item
        // Sell the item for a value higher than the price given, or a value equal to the price given
        // If there are no buyers that match your order, print "Your item has been listed"
        static void PostTransaction(List<Order> orders, string name, int price)
        {
            bool found = false;
            Random rnd = new Random();

            for (int i = 0; i < orders.Count; i++)
            {
                if (name.Equals(orders[i].Name))
                {
                    found = true;

                    if (found)
                    {
                        if (price >= orders[i].Price)
                        {
                            int newprice = rnd.Next(i, i + 2);
                            Console.WriteLine("Your order has been bought by a buyer for price:" + orders[newprice].Price);
                            break;
                        }

                        else
                        {
                            Console.WriteLine("Your item has been listed");
                            break;
                        }
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            string item;
            int price;
            List<Order> allOrders = GetDefaultOrders();
            Console.Write("Enter the name of the item you want to sell: ");
            item = Console.ReadLine();

            Console.Write("Enter the amount you want to sell the item for: ");
            price = Convert.ToInt32(Console.ReadLine());

            PostTransaction(allOrders, item, price);
            Console.ReadKey();
        }
    }
}
