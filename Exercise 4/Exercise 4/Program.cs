﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_4
{
    class Program
    {
        static bool SearchArray(string[] items, string itemToSearch)
        {
            bool found = false;

            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == itemToSearch)
                {
                    found = true;
                    break;
                }
            }
            return found;
        }


        static void Main(string[] args)
        {
            string[] items = { "Robe", "Armor", "Plate", "Staff" };
            int[] itemsqty = { 50, 10, 200, 4 };

            Console.WriteLine("Input an item to search: ");
            string input = Console.ReadLine();

            bool found = SearchArray(items, input);

            foreach (int qty in itemsqty)
            {

                if (found)
                {
                    Console.WriteLine("Found " + qty + " " + input + "s.");
                    break;
                }

                else
                {
                    Console.WriteLine(input + " was not found.");
                    break;
                }

            }
            Console.ReadLine();
        }
    }
}