﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_12__Final_
{
     class HealingAbility : Ability
    {
        public void Use()
        {
            Player player = new Player();

            if (player.ManaPoints >= 8)
            {
                player.ManaPoints -= 8;
                player.CurrentHP += 16;
            }

            else
            {
                return;
            }
        }
    }
}
