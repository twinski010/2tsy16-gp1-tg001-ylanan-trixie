﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_12__Final_
{
    class Program
    {
        static void Main(string[] args)
        {
            Monster monster = new Monster();
            Player player = new Player();

            monster.MaximumHP = 80;
            monster.CurrentHP = monster.MaximumHP;
            monster.MaxManaPoints = 20;
            monster.ManaPoints = monster.MaxManaPoints;
            monster.ATKPower = 10;

            player.MaximumHP = 50;
            player.CurrentHP = monster.MaximumHP;
            player.MaxManaPoints = 20;
            player.ManaPoints = player.MaxManaPoints;
            player.ATKPower = 10;

            do
            {
                Console.WriteLine("Monster HP:" + monster.CurrentHP + "/" + monster.MaximumHP + "\n        MP:" + monster.ManaPoints + "/" + monster.MaxManaPoints + "\n");
                Console.WriteLine("Player HP:" + player.CurrentHP + "/" + player.MaximumHP + "\n       MP:" + monster.ManaPoints + "/" + monster.MaxManaPoints + "\n");

                Console.WriteLine("Choose an action:");
                Console.WriteLine("[a] - Attack Magic");
                Console.WriteLine("[b] - Heal Magic");
                Console.WriteLine("[c] - ATK Buff Magic");
                Console.WriteLine("[d] - DEF Buff Magic");
                string action = Console.ReadLine();

                if (action.Equals("a"))
                {
                    DamageAbility a1 = new DamageAbility("Fireball", "Burns the enemy, dealing 10 damage.", 1, 6);
                }

                if (action.Equals("b"))
                {
                    //HealingAbility b1 = new HealingAbility("Cure", "Recovers 16 HP", 2.5, 8);
                }

                if (action.Equals("c"))
                {
                    //ATKBuffingAbility c1 = new ATKBuffingAbility("Attack Up", "Raises ATK Power by 6 points", 3, 3);
                }

                if (action.Equals("d"))
                {
                    //DEFBuffingAbility d1 = new DEFBuffingAbility("Defense Up", "Raises DEF Power by 6 points", 3, 3);
                }

                player.CurrentHP -= monster.ATKPower;
                Console.Clear();

            } while (player.CurrentHP > 0 && monster.CurrentHP > 0);


            if (player.CurrentHP <= 0)
            {
                Console.WriteLine("Monster Wins!");
            }

            if (monster.CurrentHP <= 0)
            {
                Console.WriteLine("Player Wins!");
            }

            Console.ReadLine();
        }
    }
}

