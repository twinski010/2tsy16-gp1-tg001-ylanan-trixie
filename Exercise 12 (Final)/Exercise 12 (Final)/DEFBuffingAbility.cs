﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_12__Final_
{
    class DEFBuffingAbility : Ability
    {
        public void Use()
        {
            Player player = new Player();

            if (player.ManaPoints >= 3)
            {
                player.ManaPoints -= 3;
                player.Defense += 6;
            }

            else
            {
                return;
            }
        }
    }
}
