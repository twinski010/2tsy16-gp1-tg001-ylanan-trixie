﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_12__Final_
{
    class DamageAbility : Ability
    {
        public void Use()
        {
            Monster monster = new Monster();
            Player player = new Player();

            if (player.ManaPoints >= 6)
            {
                player.ManaPoints -= 6;
                monster.CurrentHP -= 10;
            }

            else
            {
                return;
            }

        }
    }
}
