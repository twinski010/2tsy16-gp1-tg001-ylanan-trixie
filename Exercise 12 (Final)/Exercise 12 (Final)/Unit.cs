﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise_12__Final_
{
    class Unit
    {
        public float ATKPower { get; set; }
        public float Defense { get; set; }
        public float CurrentHP { get; set; }
        public float MaximumHP { get; set; }
        public float ManaPoints { get; set; }
        public float MaxManaPoints { get; set; }
    }
}
